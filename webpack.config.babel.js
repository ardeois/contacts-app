export default {
  entry: './client/index.js',
  output: {
    path: './server/public/lib',
    filename: 'bundle.js'
  },
  debug: true,
  devtool: 'inline-source-map',
  module: {
    loaders: [
      {
        test: /.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react'],
          plugins: ['transform-inline-environment-variables']
        }
      }
    ]
  }
};
