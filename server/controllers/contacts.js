import log from 'loglevel'
import formidable from 'formidable'
import sharp from 'sharp'
import ContactSchema from '../models/contact'

/**
 * Generates a 500 response with a json body containing the error
 * @param res A request
 * @returns {function(*=)}
 */
function handleError(res) {
  return (err) => {
    log.error(err);
    res.status(500).json({
      message: err.message
    })
  };
}

/**
 * Remove the `_id` and sets a `id` field instead
 * Change the picture to a URL
 *
 * @param item A mongo item
 * @return The transformed item
 *
 */
function transform(item) {
  const newItem = Object.assign({}, item.toJSON());
  newItem.id = item._id;
  delete newItem._id;
  if (newItem.hasOwnProperty('__v')) {
    delete newItem.__v;
  }
  newItem.picture = `api/contacts/${newItem.id}/avatar.png`;

  return newItem;
}

/**
 * Parse multipart form data and resize avatar
 * @param req
 * @returns {Promise}
 */
function transformFormData(req) {
  return new Promise((resolve, reject) => {
    const form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
      const input = fields;

      sharp(files.picture.path)
        .resize(200)
        .png()
        .toBuffer()
        .then((data) => {
          input.picture = data;
          resolve(input);
        })
        .catch((err) => {
          reject(err);
        });

    });
  });
}

const list = (req, res) => {
  ContactSchema.find(null, 'name')
    .sort({ name: 1 })
    .exec()
    .then((contacts) => {
      res.json(contacts.map(transform));
    })
    .catch(handleError(res));
};

const create = (req, res) => {
  transformFormData(req)
    .then((input) => {
      const contact = new ContactSchema(input);
      contact.save()
        .then(transform)
        .then((savedContact) => {
          res.json(savedContact);
        })
        .catch(handleError(res));
    });
};

const read = (req, res) => {
  ContactSchema.findById(req.params.id, '-picture')
    .exec()
    .then(transform)
    .then((contact) => {
      res.json(contact);
    })
    .catch(handleError(res));
};

const avatar = (req, res) => {
  ContactSchema.findById(req.params.id, 'picture')
    .exec()
    .then((contact) => {
      res.contentType('image/png');
      res.send(contact.picture);
    })
    .catch(handleError(res));
};

const update = (req, res) => {
  ContactSchema.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  })
    .exec()
    .then(transform)
    .then((contact) => {
      res.json(contact);
    })
    .catch(handleError(res));
};

const remove = (req, res) => {
  ContactSchema.findById(req.params.id)
    .exec()
    .then((contact) => contact.remove())
    .then(() => {
      res.status(204).send();
    })
    .catch(handleError(res));
};

export default { list, create, read, update, remove, avatar };
