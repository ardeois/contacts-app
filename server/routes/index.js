import api from './api'
import path from 'path'

export default (app) => {
  app.use('/api/', api);

  app.get('/lib/:file', (req, res) => {
    res.sendFile(path.join(__dirname, '../public/lib', req.params.file));
  });
  
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../public', 'index.html'));
  });
};
