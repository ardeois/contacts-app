import { Router } from 'express'
import Contacts from '../controllers/contacts'

const router = Router();

router.get('/contacts', Contacts.list);
router.post('/contacts', Contacts.create);
router.get('/contacts/:id', Contacts.read);
router.get('/contacts/:id/avatar.png', Contacts.avatar);
router.put('/contacts/:id', Contacts.update);
router.delete('/contacts/:id', Contacts.remove);

export default router;
