import mongoose from 'mongoose'

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PHONE_REGEX = /^\d[\d -]*$/;

const contactSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  job_title: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  phone_number: {
    type: String,
    required: true,
    validate: {
      validator: (value) => {
        return PHONE_REGEX.test(value);
      },
      message: '{VALUE} is not a valid phone number'
    }
  },
  email: {
    type: String,
    required: true,
    validate: {
      validator: (value) => {
        return EMAIL_REGEX.test(value);
      },
      message: '{VALUE} is not a valid email'
    }
  },
  picture: {
    type: Buffer,
    require: true
  }
});

const Contact = mongoose.model('Contact', contactSchema);

export default Contact;
