/*eslint no-process-env: "off"*/
import mongoose from 'mongoose'
import express from 'express'
import bodyParser from 'body-parser'
import config from './config'
import initRoutes from './routes'
import log from 'loglevel';

mongoose.Promise = global.Promise;

log.setLevel('INFO');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

initRoutes(app);

function listen() {
  const port = process.env.PORT || config.port;
  app.listen(port);
  console.log('Express app started on port ' + port);
}

function connect() {
  const mongo_user = process.env.MONGO_USER || config.mongo_user;
  const mongo_password = process.env.MONGO_PASSWORD || config.mongo_password;
  const mongo_host = process.env.MONGO_HOST || config.mongo_host;
  const mongo_port = process.env.MONGO_PORT || config.mongo_port;
  const mongo_database = process.env.MONGO_DATABASE || config.mongo_database;

  const mongodb = `mongodb://${mongo_user}:${mongo_password}@${mongo_host}:${mongo_port}/${mongo_database}`;

  var options = { server: { socketOptions: { keepAlive: 1 } } };
  return mongoose.connect(mongodb, options).connection;
}

connect()
  .on('error', console.log)
  .on('disconnected', connect)
  .once('open', listen);
