# Contacts App

A simple app to manage contacts in ES6 using Express and React/Redux.


# Demo

https://damp-atoll-81249.herokuapp.com/

# Getting started

```
npm install
npm start
```

# API

|       Request              |      Description     |
|----------------------------|----------------------|
| `GET /api/contacts`        | List all contacts    |
| `POST /api/contacts`       | Create a new contact |
| `GET /api/contacts/:id`    | Get a contact        |
| `PUT /api/contacts/:id`    | Update a contact     |
| `DELETE /api/contacts/:id` | Delete a contact     |

## GET /api/contacts
   
### Request

```
 GET http://server:port/api/contacts
```

### Response

```application/json
  [
      {
          "name": "John Doe",
          "id": "581a375f9eeba50bddbe84d6"
      },
      ...
  ]
```

## POST /api/contacts
   
### Request
```
 POST http://server:port/api/contacts
```
Request should be a multipart form data including those fields:
```
address="Montreal"
email="john@doe.com"
job_title="Unknown"
name="John Doe"
phone_number="11111111"
picture=$fileData$
```

### Response

```application/json
{
    "address": "Montreal",
    "phone_number": "1111111",
    "email": "john.doe@example.com",
    "name": "John Doe",
    "job_title": "Unknown",
    "picture": "api/contacts/581bcd2fe2dd6388fa813655/avatar.png",
    "id": "581bcd2fe2dd6388fa813655"
}
```

## GET /api/contacts/:id
   
### Request
```
 GET http://server:port/api/contacts/:id
```

### Response

```application/json
   {
      address: "Montreal"
      email: "john@doe.com"
      id: "581bc854f61dd584f515de79"
      job_title: "Unknown"
      name: "John Doe"
      phone_number: "11111111"
      picture: "api/contacts/581bc854f61dd584f515de79/avatar.png"
   }
```

## PUT /api/contacts/:id
   
### Request
```
 GET http://server:port/api/contacts/:id
 
 {
    address: "Montreal"
    email: "john@doe.com"
    job_title: "Unknown"
    name: "John Doe"
    phone_number: "11111111"
 }
```

### Response

```application/json
   {
      address: "Montreal"
      email: "john@doe.com"
      id: "581bc854f61dd584f515de79"
      job_title: "Unknown"
      name: "John Doe"
      phone_number: "11111111"
      picture: "api/contacts/581bc854f61dd584f515de79/avatar.png"
   }
```

## DELETE /api/contacts/:id
   
### Request
```
 DELETE http://server:port/api/contacts/:id
```

### Response
 code `204`, no content

# Tests

```
npm run test
```

# License

ISC License
