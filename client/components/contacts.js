import React, { Component, PropTypes } from 'react'
import { List } from 'material-ui/List';
import ContactItem from './contactItem'

class Contacts extends Component {
  render() {
    const style = {
      list: {
        overflowY: 'auto'
      }
    };
    return <List style={style.list}>{
        this.props.contacts.map((contact) => {
          return <ContactItem onClick={() => { this.props.viewContact(contact.id) } }
                              key={contact.id}
                              contact={contact}/>;
        })
      }</List>
  }
}

Contacts.propTypes = {
  viewContact: PropTypes.func.isRequired,
  contacts: PropTypes.array.isRequired,
};

export default Contacts;

