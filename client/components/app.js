import React, { Component } from 'react';
import LeftBar from '../containers/leftBar'
import RightPage from '../containers/rightPage'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class App extends Component {
  render() {
    return <MuiThemeProvider>
      <div>
        <LeftBar/>
        <RightPage/>
      </div>
    </MuiThemeProvider>;
  }
}

export default App;
