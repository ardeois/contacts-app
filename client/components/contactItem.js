import React, { Component, PropTypes } from 'react'
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';

const ContactItem = props => {
    return <ListItem
      onClick={props.onClick}
      primaryText={props.contact.name}
      leftAvatar={<Avatar src={props.contact.picture} />}
    />;
};

ContactItem.propTypes = {
  onClick: PropTypes.func.isRequired,
  contact: PropTypes.object.isRequired
};

export default ContactItem;

