import React from 'react'
import chai, { expect } from 'chai'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'
import ContactItem from './contactItem'
import { ListItem } from 'material-ui/List';
chai.should();
chai.use(sinonChai);

describe('<Contact/>', () => {
  const contact = {
    id: 'abc',
    name: 'John Doe'
  };

  it('Should render properly', () => {
    const wrapper = shallow(<ContactItem contact={contact} onClick={() => {} }/>);

    const item = wrapper.find(ListItem);
    expect(item.at(0).props().primaryText).to.equal('John Doe');
  });

  it('Should trigger viewContact when clicking', () => {
    const viewContactSpy = sinon.spy();
    const wrapper = shallow(<ContactItem contact={contact} onClick={viewContactSpy}/>);

    wrapper.simulate('click');
    expect(viewContactSpy).to.have.been.called;
  });
});
