import React from 'react'
import chai, { expect } from 'chai'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'
import Contacts from './contacts'
import ContactItem from './contactItem'
chai.should();
chai.use(sinonChai);

describe('<Contacts/>', () => {
  const contactList = [
    {
      id: 'abc',
      name: 'John Doe'
    },
    {
      id: 'def',
      name: 'Jane Doe'
    }
  ];

  it('Should render <Contact/> components', () => {
    const wrapper = shallow(<Contacts contacts={contactList} viewContact={() => {} }/>);

    const allInputs = wrapper.find(ContactItem);
    expect(allInputs).to.be.length(2);
    expect(allInputs.at(0).props().contact.name).to.equal('John Doe');
    expect(allInputs.at(1).props().contact.name).to.equal('Jane Doe');
  });

  it('Should trigger viewContact with id', () => {
    const viewContactSpy = sinon.spy();
    const wrapper = shallow(<Contacts contacts={contactList} viewContact={viewContactSpy}/>);

    const allInputs = wrapper.find(ContactItem);
    expect(allInputs).to.be.length(2);
    expect(allInputs.at(0).props().contact.name).to.equal('John Doe');

    allInputs.at(0).props().onClick();
    expect(viewContactSpy).to.have.been.calledWith('abc');

    allInputs.at(1).props().onClick();
    expect(viewContactSpy).to.have.been.calledWith('def');

  });
});
