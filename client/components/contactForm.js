import React, { Component, PropTypes } from 'react'
import { Field, reduxForm } from 'redux-form'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import Dropzone from 'react-dropzone'

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PHONE_REGEX = /^\d[\d -]*$/;

const validate = values => {
  const errors = {};
  if (!values.name) {
    errors.name = 'Required'
  }
  if (!values.job_title) {
    errors.job_title = 'Required'
  }
  if (!values.address) {
    errors.address = 'Required'
  }
  if (!values.email) {
    errors.email = 'Required'
  } else if (!EMAIL_REGEX.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.phone_number) {
    errors.phone_number = 'Required'
  } else if (!PHONE_REGEX.test(values.phone_number)) {
    errors.phone_number = 'Invalid phone number'
  }

  return errors;
};

const renderTextField = ({ input, label, disabled, meta: { touched, error } }) => {
  return (<TextField hintText={label}
                     floatingLabelText={label}
                     errorText={touched && error}
                     disabled={disabled}
                     {...input} />);
};

const renderDropZone = ({ input }) => {
  const style = {
    width: 100,
    height: 50,
    borderWidth: 2,
    borderColor: '#666',
    borderStyle: 'dashed',
    borderRadius: 5,
    margin: 10
  };
  const activeStyle = {
    borderStyle: 'solid',
    backgroundColor: '#eee'
  };
  const rejectStyle = {
    borderStyle: 'solid',
    backgroundColor: '#ffdddd'
  };
  const onChange = (files) => {
    if (files.length > 0) {
      input.onChange(files[0]);
    }
  };
  return (<Dropzone accept="image/*" onDrop={onChange} style={style} activeStyle={activeStyle} rejectStyle={rejectStyle}>
    { input.value ?
      input.value.name :
      <div>Drop avatar, or click.</div>
    }
  </Dropzone>)
};

const ContactForm = props => {
  const { handleSubmit, onDelete, pristine, submitting, style, readOnly, initialValues, fields } = props;

  return <form onSubmit={handleSubmit} style={style}>
    { initialValues ?
      null :
      <Field name="picture" component={renderDropZone} label="Picture" />
    }
    <div>
      <Field name="name" component={renderTextField} label="Name" disabled={readOnly}/>
    </div>
    <div>
      <Field name="job_title" component={renderTextField} label="Job Title" disabled={readOnly}/>
    </div>
    <div>
      <Field name="address" component={renderTextField} label="Address" disabled={readOnly}/>
    </div>
    <div>
      <Field name="phone_number" component={renderTextField} label="Phone" disabled={readOnly}/>
    </div>
    <div>
      <Field name="email" component={renderTextField} label="Email" disabled={readOnly}/>
    </div>
    { readOnly ?
      null :
      <RaisedButton primary={true} disabled={pristine || submitting} onClick={handleSubmit}>{initialValues? 'Save' : 'Add'}</RaisedButton>
    }
    { !readOnly && initialValues ?
      <RaisedButton secondary={true} onClick={handleSubmit(onDelete)}>Delete</RaisedButton> :
      null
    }
  </form>;
};


ContactForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onDelete: PropTypes.func
};

export const NewContactForm = reduxForm({
  form: 'NewContactForm',
  validate
})(ContactForm);

export const EditContactForm = reduxForm({
  form: 'EditContactForm',
  validate
})(ContactForm);
