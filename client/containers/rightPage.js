import React, { Component } from 'react';
import { connect } from 'react-redux'
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import Avatar from 'material-ui/Avatar';
import { NewContactForm, EditContactForm }from '../components/contactForm'
import * as ContactActions from '../actions/contacts'

class Page extends Component {
  render() {
    const { contact, isAdding, isEditing, viewEditContact } = this.props;
    const style = this.style;

    return <div style={style.root}>
        <AppBar title={this.appBarTitle}
                showMenuIconButton={contact ? true : false}
                iconElementLeft={contact ?
                  <Avatar src={contact.picture} /> :
                  null
                }
                iconElementRight={ contact && !isAdding && !isEditing ?
                  <FlatButton label="Edit" onClick={viewEditContact}/> :
                  null
                }
                style={style.appBar}/>
      { this.contactPage }
    </div>;
  }

  get style() {
    return {
      root: {
        paddingLeft: 300
      },
      contact: {
        padding: 20
      }
    };
  }

  get appBarTitle() {
    let title = '';
    if (this.props.isAdding) {
      title = 'New Contact';
    } else if (this.props.contact) {
      title = this.props.contact.name;
    }
    return title;
  }

  get contactPage() {
    const { contact, isAdding, isEditing, onAdd, onEdit, onDelete } = this.props;
    const style = this.style;
    if (!contact && !this.props.isAdding) {
      return null;
    }
    return (isAdding ?
      <NewContactForm onSubmit={onAdd} readOnly={false} style={style.contact}/> :
      <EditContactForm onSubmit={onEdit} onDelete={onDelete} initialValues={contact} readOnly={!isEditing} style={style.contact}/>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    contact: state.contact.item,
    isEditing: state.contact.isEditing,
    isAdding: state.contact.isAdding
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    viewEditContact: () => {
      dispatch(ContactActions.viewEditContact());
    },
    onAdd: (contact) => {
      dispatch(ContactActions.addContact(contact));
    },
    onEdit: (contact) => {
      dispatch(ContactActions.editContact(contact));
    },
    onDelete: (contact) => {
      dispatch(ContactActions.deleteContact(contact.id));
    },
  }
};

const RightPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(Page);

export default RightPage;
