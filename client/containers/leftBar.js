import React, { Component } from 'react';
import { connect } from 'react-redux'
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import Contacts from '../components/contacts'
import { viewContact, viewAddContact } from '../actions/contacts'

class Bar extends Component {
  render() {
    const style = {
      appBar: {
      },
      drawer: {
        height: '100%'
      },
      contacts: {
        marginTop: 70
      }
    };
    return <div>
      <Drawer width={300} open={true} docker={true} style={style.drawer}>
        <AppBar title="Contacts"
                showMenuIconButton={false}
                iconElementRight={<FlatButton label="Add" onClick={this.props.viewAddContact}/>}
                style={style.appBar}/>
        {this.props.isFetching ?
          'Loading...' :
          <Contacts contacts={this.props.contacts} viewContact={this.props.viewContact}/>
        }
      </Drawer>
    </div>;
  }
}

const mapStateToProps = (state) => {
  return {
    contacts: state.contacts.items,
    isFetching: state.contacts.isFetching
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    viewContact: (id) => {
      return dispatch(viewContact(id));
    },
    viewAddContact: () => {
      return dispatch(viewAddContact());
    }
  }
};

const LeftBar = connect(
  mapStateToProps,
  mapDispatchToProps
)(Bar);

export default LeftBar;
