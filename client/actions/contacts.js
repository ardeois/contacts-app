import { createAction } from 'redux-actions'
import Api from '../api/contacts';

const api = new Api(process.env.API_URL || 'http://localhost:8080');


/**
 * Actions using redux-actions and thunk
 *
 * 2 actions are created for async calls (ACTION_NAME and RECEIVE_ACTION_NAME)
 * Actions that triggers other actions when the API call is completed might now have
 * a RECEIVE_ACTION_NAME action
 *
 */

// List contact
export const listContacts = createAction('LIST_CONTACTS', () => {
  return (dispatch) => {
    api.list().then((contacts) => {
      dispatch(receiveListContacts(contacts));
    });
  };
});

export const receiveListContacts = createAction('RECEIVE_LIST_CONTACTS');

// Add contact
export const viewAddContact = createAction('VIEW_ADD_CONTACT');

export const addContact = createAction('ADD_CONTACT', (contact) => {
  return (dispatch) => {
    return api.add(contact)
      .then((result) => {
        dispatch(listContacts());
        dispatch(viewContact(result.id));
        return result;
      });
  }
});

// View contact
export const viewContact = createAction('VIEW_CONTACT', (id) => {
  return (dispatch) => {
    if (id) {
      api.get(id)
        .then((result) => {
          dispatch(receiveViewContact(result));
        });
    } else {
      dispatch(receiveViewContact(null));
    }

  };
});

export const receiveViewContact = createAction('RECEIVE_VIEW_CONTACT');


//Edit contact
export const viewEditContact = createAction('VIEW_EDIT_CONTACT');

export const editContact = createAction('EDIT_CONTACT', (contact) => {
  return (dispatch) => {
    api.update(contact)
      .then((result) => {
        dispatch(listContacts());
        dispatch(viewContact(result.id));
      });
  }
});

//Delete contact
export const deleteContact = createAction('DELETE_CONTACT', (id) => {
  return (dispatch) => {
    api.remove(id)
      .then(() => {
        dispatch(listContacts());
        dispatch(viewContact(null));
      });
  }
});
