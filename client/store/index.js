import { createStore, applyMiddleware } from 'redux'
import fsaThunkMiddleware from 'redux-fsa-thunk';
import reducer from '../reducers'
import { listContacts } from '../actions/contacts';

const store = createStore(reducer,
  applyMiddleware(fsaThunkMiddleware)
);

store.dispatch(listContacts());

export default store;
