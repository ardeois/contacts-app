import log from 'loglevel'

class Api {
  constructor(apiUrl) {
    this.apiUrl = apiUrl;
  }

  get headers() {
    return {
      'Content-Type': 'application/json'
    }
  };

  httpRequest(method, url, body, useFormData=false) {

    const init = { method};

    if (method !== 'GET' && body) {
      if (useFormData) {
        const formData = new FormData();

        Object.keys(body).map(function (key) {
          var value = body[key];
          formData.append(key, value);
        });
        init.body = formData;
      } else {
        init.body = JSON.stringify(body);
        init.headers = this.headers;
      }
    }
    url = `${this.apiUrl}${url}`;

    const request = new Request(url, init);

    // Build the wrapper promise.
    return new Promise((resolve, reject) => {
      log.debug('-->', `HTTP ${method}`, url, JSON.stringify(init.headers), JSON.stringify(body));
      let promise = fetch(request.url, init);

      promise.then((response) => {
        log.debug('<--', `HTTP ${response.status}`);
        if (response.status >= 400) {
          return reject(response);
        } else {
          return response;
        }
      });

      promise.then((response) => {
        return response.status !== 204 ? resolve(response.json()) : resolve(null);
      }, (error) => reject(error.json()));
    });
  }

  list() {
    return this.httpRequest('GET', '/api/contacts');
  }

  get(id) {
    return this.httpRequest('GET', `/api/contacts/${id}`);
  }

  add(contact) {
    return this.httpRequest('POST', '/api/contacts', contact, true);
  }

  update(contact) {
    contact = Object.assign({}, contact);
    delete contact.picture;
    return this.httpRequest('PUT', `/api/contacts/${contact.id}`, contact);
  }

  remove(id) {
    return this.httpRequest('DELETE', `/api/contacts/${id}`);
  }
}

export default Api;
