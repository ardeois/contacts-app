import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import App from './components/app'
import store from './store'
import log from 'loglevel'
import injectTapEventPlugin from 'react-tap-event-plugin'

log.setLevel('DEBUG');

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();


ReactDOM.render(<Provider store={store}>
  <App/>
</Provider>, document.getElementById('app'));
