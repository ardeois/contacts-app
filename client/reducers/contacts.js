import { handleActions } from 'redux-actions'
import * as ContactActions from '../actions/contacts'

const initialState = {
  isFetching: false,
  items: []
};

const reducer = handleActions({
    [ContactActions.listContacts]: {
      next: (state, actions) => {
        return {
          isFetching: true,
          items: state.items
        };
      },
      throw: () => initialState
    },
    [ContactActions.receiveListContacts]: {
      next: (state, actions) => {
        return {
          isFetching: false,
          items: [...actions.payload]
        };
      },
      throw: () => initialState
    }
  },
  initialState);

export default reducer;
