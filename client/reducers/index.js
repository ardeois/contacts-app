import { combineReducers } from 'redux'
import contacts from './contacts'
import contact from './contact'
import { reducer as reduxFormReducer } from 'redux-form'

export default combineReducers({ contacts, contact, form: reduxFormReducer });
