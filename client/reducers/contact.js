import { handleActions } from 'redux-actions'
import * as ContactActions from '../actions/contacts'

const initialState = {
  isEditing: false,
  isAdding: false,
  item: null
};

const reducer = handleActions({
    [ContactActions.viewContact]: {
      next: (state, actions) => {
        return {
          isFetching: true,
          isEditing: false,
          isAdding: false,
          item: actions.payload
        };
      },
      throw: () => state
    },
    [ContactActions.receiveViewContact]: {
      next: (state, actions) => {
        return {
          isFetching: false,
          isEditing: false,
          isAdding: false,
          item: actions.payload
        };
      },
      throw: () => state
    },
    [ContactActions.viewEditContact]: {
      next: (state, actions) => {
        return {
          isEditing: true,
          isAdding: false,
          item: state.item
        };
      },
      throw: () => state
    },
    [ContactActions.viewAddContact]: {
      next: (state, actions) => {
        return {
          isEditing: false,
          isAdding: true,
          item: null
        };
      },
      throw: () => state
    },
  },
  initialState
);

export default reducer;
