import { expect } from 'chai'
import { createAction } from 'redux-actions'
import reducer from '../../client/reducers/contacts'

describe('reducer/contacts', function() {
  const initialState = {
    isFetching: false,
    items: []
  };
  const aContactEntry = (index) => {
    return {
      id: index,
      name: 'John Doe'
    }
  };

  describe('action LIST_CONTACTS', () => {
    const listContacts = createAction('LIST_CONTACTS');

    it('should return the list from payload', () => {
      const newState = reducer(initialState, listContacts());

      expect(newState.isFetching).to.eql(true);
      expect(newState.items).to.have.length(0);
    });
  });

  describe('action RECEIVE_LIST_CONTACTS', () => {
    const receiveListContacts = createAction('RECEIVE_LIST_CONTACTS');

    it('should return the list from payload', () => {
      const newState = reducer(initialState, receiveListContacts([
        aContactEntry('1234')
      ]));

      expect(newState.isFetching).to.eql(false);
      expect(newState.items).to.have.length(1);
      expect(newState.items[0]).to.eql({
        id: '1234',
        name: 'John Doe'
      })
    });
  });
});
